<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
        ));
    }

    /**
     * @Route("fileUpload/")
     *
     * @return JsonResponse
     */
    public function uploadAction(Request $request)
    {

//        print_r($request->files->get("file"));
        $file = $request->files->get("file");

        $fileName = md5(uniqid()).'.'.$file->guessExtension();

        // Move the file to the directory where brochures are stored
        $fileDir = $this->container->getParameter('kernel.root_dir').'/../web/uploads';
        $file->move($fileDir, $fileName);

        return new JsonResponse(array("fileName"=>$file->getPathname(),"originalFileName"=>$file->getClientOriginalName()));



    }




}
